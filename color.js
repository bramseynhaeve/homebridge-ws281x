const convert = require('color-convert')

class Color {
    constructor(red = 0, green = 0, blue = 0, brightness = 1) {
        this.red = red
        this.green = green
        this.blue = blue
        this.brightness = brightness
    }

    get rawColor() {
        let limitedBrightness = Math.max(0, Math.min(this.brightness, 1))
        let color = new Object()

        color.red = Math.max(0, Math.min(this.red * limitedBrightness, 255)) 
        color.green = Math.max(0, Math.min(this.green * limitedBrightness, 255))
        color.blue = Math.max(0, Math.min(this.blue * limitedBrightness, 255))

        let colorString = '0x' + convert.rgb.hex(color.red, color.green, color.blue)
        return parseInt(colorString)
    }
}

module.exports = Color