let Service, Characteristic;

const Strip = require('rpi-ws281x-native')
const convert = require('color-convert')
const Pixel = require('./pixel')
const Color = require('./color')
const Constants = require('./constants')

class NeoPixelAccessory {
    constructor (log, config) {
        this.log = log
        this.name = config["name"]
        this.numberOfPixels = parseInt(config["pixels"]) || 10 // fallback setting to 10 pixels to prevent overloading the raspberry pi
        
        this.hue = 0
        this.saturation = 0
        this.brightness = 0
        this.power = false
        
        this.pixels = []
        
        for (var i = 0; i < this.numberOfPixels; i++) {
            this.pixels[i] = new Pixel(i, this.numberOfPixels)
        }
        
        Strip.init(this.numberOfPixels);
        Strip.render(stripColors(this.pixels))
        Strip.setBrightness(255)
        
        this.informationService = new Service.AccessoryInformation()
        this.informationService.setCharacteristic(Characteristic.Manufacturer, "Bram")
        this.informationService.setCharacteristic(Characteristic.Model, "Neopixel Ledstrip")
        this.informationService.setCharacteristic(Characteristic.SerialNumber, "030-061-993")
        
        this.ledService = new Service.Lightbulb(this.name)
        
        this.ledService
        .getCharacteristic(Characteristic.On)
        .on('set', this.setPower.bind(this))
        .on('get', this.getPower.bind(this))
        
        this.ledService
        .getCharacteristic(Characteristic.Brightness)
        .on('set', this.setBrightness.bind(this))
        .on('get', this.getBrightness.bind(this))
        
        this.ledService
        .getCharacteristic(Characteristic.Hue)
        .on('get', this.getHue.bind(this))
        .on('set', this.setHue.bind(this))
        
        this.ledService
        .getCharacteristic(Characteristic.Saturation)
        .on('get', this.getSaturation.bind(this))
        .on('set', this.setSaturation.bind(this))
        
        this.updateTimer = setInterval(this.updateLoop.bind(this), Constants.dutyCycleTime)
    }
    
    // Color handling
    updateLedColor() {
        const colorValues = convert.hsv.rgb(this.hue, this.saturation, 100)
        let brightnessPercentage = this.brightness / 100
        
        if (this.power == false) {
            brightnessPercentage = 0
        }

        if (brightnessPercentage > 0) {
            brightnessPercentage = 0.1 + (brightnessPercentage * 0.9)
        }

        let currentColor = new Color(colorValues[0], colorValues[1], colorValues[2], brightnessPercentage)
        this.pixels.forEach(pixel => {
            pixel.animateToColor(currentColor)
        })
    }
    
    // Update loop
    updateLoop() {
        Strip.render(stripColors(this.pixels))
    }
    
    // --------------------------------------------------------------
    // --------------------- HOME BRIDGE SETTER ---------------------
    // --------------------------------------------------------------
    
    // POWER
    getPower(callback) {
        this.log("Get Power: " + this.power)
        callback(null, this.power)
    }
    
    setPower(power, callback) {
        this.log("Set power:" + power)        
        this.power = power
        this.updateLedColor()
        callback()
    }
    
    // Brightness
    getBrightness(callback) {
        this.log("Get brightness: " + this.brightness)
        callback(null, this.brightness)
    }
    
    setBrightness(brightness, callback) {
        this.log("Set brightness: " + brightness)
        this.brightness = brightness
        this.updateLedColor()
        callback()
    }
    
    // Hue
    getHue(callback) {
        this.log("Get hue: " + this.hue)
        callback(null, this.hue)
    }
    
    setHue(hue, callback) {
        this.log("Set hue: " + hue)
        this.hue = hue
        this.updateLedColor()
        callback()
    }
    
    // Saturation
    getSaturation(callback) {
        this.log("Get saturation: " + this.saturation)
        callback(null, this.saturation)
    }
    
    setSaturation(saturation, callback) {
        this.log("Set saturation: " + saturation)
        this.saturation = saturation
        this.updateLedColor()
        callback()
    }
    
    // Services
    getServices() {
        return [this.informationService, this.ledService]
    }
}

// HELPERS
function stripColors(array) {
    return array.map(pixel => pixel.color.rawColor)
}

module.exports = function(homebridge) {
    Service = homebridge.hap.Service
    Characteristic = homebridge.hap.Characteristic
    homebridge.registerAccessory("homebridge-neopixel", "NeoPixel", NeoPixelAccessory)
}