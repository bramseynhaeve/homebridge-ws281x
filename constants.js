class Constants {
}

Constants.frequency = 25
Constants.dutyCycleTime = 1 / Constants.frequency * 1000

module.exports = Constants