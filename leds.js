var NUMBER_OF_LEDS = 300; // 32 leds in the Unicorn pHat

var strip = require('rpi-ws281x-native');
strip.init(NUMBER_OF_LEDS);
strip.setBrightness(50); // A value between 0 and 255

// The RGB colours of the LEDs. for instance 0xff0000 is red, 0x0000ff is blue, etc.
var leds = [
    0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0    
];

var ledIndex = 0
var tryCount = 0

// Loop every 0.5 sec: randomise an index and a colour and display it
let duration = 50
strip.render(leds);
var interval = setInterval(function() {
    var color =  0xffffff

    for (var i = 0; i < NUMBER_OF_LEDS; i++) {
        leds[i] = (ledIndex == i) ? 0xffffff : 0
    }

    console.log("i -> " + ledIndex)

    ledIndex ++
    if (ledIndex >= NUMBER_OF_LEDS) {
        ledIndex = 0
        tryCount ++
        console.log("retry")
    }

    if (tryCount > 2) {
        console.log("stop")
        process.exit(1)
    }

    strip.render(leds);
}, duration);

// After 10 secondes, stop.
// setTimeout(function () {
//     console.log('Stop!');
//     clearInterval(interval);
//     strip.reset();
// }, 10000);