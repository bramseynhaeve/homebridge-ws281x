const Color = require('./color')
const Easing = require('easing')

const Constants = require('./constants')

class Pixel {
    constructor(index, count, color = new Color()) {
        this.index = index
        this.count = count
        this.color = color

        this.timeouts = []
    }

    animateToColor(color) {
        this.clearTimeouts()

        const redDiff = color.red - this.color.red
        const greenDiff = color.green - this.color.green
        const blueDiff = color.blue - this.color.blue
        const brightnessDiff = color.brightness - this.color.brightness
        const animationOffsetRange = 1

        let invertEasing = brightnessDiff < 0

        const offsetEasing = Easing(this.count, 'sinusoidal', {endToEnd:true, invert:invertEasing})
        let pixelPositionDurationOffset = offsetEasing[this.index] * animationOffsetRange

        const animationDuration = 0.5 + pixelPositionDurationOffset
        const numberOfLoops = Math.ceil(Constants.frequency * animationDuration) + 1
        const easingValues = Easing(numberOfLoops, 'cubic') //quadratic, cubic, quartic, quintic, exponential

        for(var i = 0; i < numberOfLoops; i ++) {
            const easingFactor = easingValues[i]
            const newRed = this.color.red + (redDiff * easingFactor)
            const newGreen = this.color.green + (greenDiff * easingFactor)
            const newBlue = this.color.blue + (blueDiff * easingFactor)
            const newBrightness = this.color.brightness + (brightnessDiff * easingFactor)
            const isLastTimeout = i == numberOfLoops - 1

            const colorTimout = setTimeout(() => {
                this.color = new Color(newRed, newGreen, newBlue, newBrightness)

                if (isLastTimeout) {
                    this.clearTimeouts()
                }
            }, Constants.dutyCycleTime * i);

            this.timeouts.push(colorTimout)
        }


    }

    clearTimeouts() {
        this.timeouts.forEach(timeout => {
            clearTimeout(timeout)
        })

        this.timeouts = []
    }
}

module.exports = Pixel